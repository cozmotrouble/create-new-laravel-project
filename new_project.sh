echo "Enter project name: "
# get the name of the project
read input_variable

# Create new Laravel project
laravel new $input_variable

# copy helper scripts to project
# s.sh starts the builtin Laravel server
cp  -f s.sh  $input_variable/s.sh
# b.sh opens the project in the default web browser
cp  -f b.sh  $input_variable/b.sh
# push.sh creates a Git commit and Pushes changes to the server
cp  -f push.sh  $input_variable/push.sh

# Update the .env with the name of the soon to be created database 
php ./updateenv.php $input_variable

# cd into the project
cd $input_variable

# Open Sublime text -- Optional
#"C:\Program Files\Sublime Text 3\sublime_text.exe" .env

# Create the database
# Note chage these to a user who can create databases  -uPRIVILEGEDUSER -pPASSWORD
# Ex: -uroot is the username "root" and -ppassword is the paswword "password"
mysql -uPRIVILEGEDUSER -pPASSWORD -e "CREATE DATABASE IF NOT EXISTS $input_variable"

# Add authentication
php artisan make:auth

# Run Migration
php artisan migrate

# Start the server
start ./s.sh

# Open project in the browser 
start ./b.sh

# open Git bash in the projects root directory -- Optional
#"C:\Program Files\Git\git-bash.exe"
  
# let the user know the project has been created
read -rsp $'Done. \n\nPress any key to continue...\n' -n1 key