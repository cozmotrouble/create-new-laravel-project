# Create new Laravel Project
---
![alt text](https://bytebucket.org/cozmotrouble/create-new-laravel-project/raw/668f33d6949883cb6e0242d3e79e8d17ccc60436/images/allthethings.png?token=847a695e5bc4da9069cd30c46aa4fae11b31cdc6 "Automate all the things")
 
---

##LICENSE
Apache License, Version 2.0.

##SYSTEM REQUIREMENTS
- Composer - https://getcomposer.org/
- Laravel  https://laravel.com/docs/5.3/installation
- PHP 5 -- Make sure that the PHP bin directory is in your system PATH  https://www.google.com/search?q=add+path+windows
- Git Bash or other Bash Shell  - https://git-scm.com/downloads

Tested on Windows 10 Pro using PHP 5.6.x (Xampp) and the latest(-ish) versions of the above as of 10/22/16

##INSTALLATION

Unzip in root of your development directory.

Change the username and password in new_project.sh to a MySQL user who can create databases
 # Ex: -uroot is username "root" and -ppassword is paswword "password"
 
********************************************************************************
Copyright 2016  Gerald Guido